export interface Embarcacao {
    id?: number
    numeroInscricao:number
    nomeEmbarcacao:string
    qtdPassageiros:number
    valorPassagem:number
    cpfProprietario: string
    nomeProprietario:string
}