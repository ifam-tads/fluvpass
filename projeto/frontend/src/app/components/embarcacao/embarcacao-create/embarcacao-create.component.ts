import { Embarcacao } from '../embarcacao.model';
import { EmbarcacaoService } from '../embarcacao.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-embarcacao-create',
  templateUrl: './embarcacao-create.component.html',
  styleUrls: ['./embarcacao-create.component.css']
})
export class EmbarcacaoCreateComponent implements OnInit {

  embarcacao: Embarcacao = {
    numeroInscricao:null,
    nomeEmbarcacao: '',
    qtdPassageiros:null,
    valorPassagem: null,
    cpfProprietario:'',
    nomeProprietario:'',

  }

  constructor(private embarcacaoService: EmbarcacaoService,
      private router: Router) { }

  ngOnInit(): void {    
  }

  createEmbarcacao(): void {
    this.embarcacaoService.create(this.embarcacao).subscribe(() => {
      this.embarcacaoService.showMessage('Embarcação criado com sucesso!')
      this.router.navigate(['/embarcacao'])
    })

  }

  cancel(): void {
    this.router.navigate(['/embarcacao'])
  }
}
