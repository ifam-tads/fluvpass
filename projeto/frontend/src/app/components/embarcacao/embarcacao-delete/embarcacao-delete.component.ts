import { Router, ActivatedRoute } from "@angular/router";
import { EmbarcacaoService } from "../embarcacao.service";
import { Embarcacao } from "../embarcacao.model";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-embarcacao-delete",
  templateUrl: "./embarcacao-delete.component.html",
  styleUrls: ["./embarcacao-delete.component.css"],
})
export class EmbarcacaoDeleteComponent implements OnInit {
  embarcacao: Embarcacao;

  constructor(
    private embarcacaoService: EmbarcacaoService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.embarcacaoService.readById(id).subscribe((embarcacao) => {
      this.embarcacao = embarcacao;
    });
  }

  deleteEmbarcacao(): void {
    this.embarcacaoService.delete(this.embarcacao.id).subscribe(() => {
      this.embarcacaoService.showMessage("Embarcação excluido com sucesso!");
      this.router.navigate(["/embarcacao"]);
    });
  }

  cancel(): void {
    this.router.navigate(["/embarcacao"]);
  }
}
