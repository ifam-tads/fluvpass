import { EmbarcacaoService } from "../embarcacao.service";
import { Embarcacao } from "../embarcacao.model";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-embarcacao-read",
  templateUrl: "./embarcacao-read.component.html",
  styleUrls: ["./embarcacao-read.component.css"],
})
export class EmbarcacaoReadComponent implements OnInit {
  embarcacao: Embarcacao[];
  displayedColumns = ["id", "name_emb", "name", "qtd_pass", "price", "action"];

  constructor(private embarcacaoervice: EmbarcacaoService) {}

  ngOnInit(): void {
    this.embarcacaoervice.read().subscribe((embarcacao) => {
      this.embarcacao = embarcacao;
    });
  }
  filtrar(palavraChave: string) {
    if (palavraChave) {
      palavraChave = palavraChave.toUpperCase();

      this.embarcacao = this.embarcacao.filter(
        (a) => a.nomeEmbarcacao.toUpperCase().indexOf(palavraChave) >= 0
      );
    }
  }

}
