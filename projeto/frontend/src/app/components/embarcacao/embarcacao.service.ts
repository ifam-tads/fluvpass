import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { HttpClient } from "@angular/common/http";
import { Embarcacao } from "./embarcacao.model";
import { Observable, EMPTY } from "rxjs";
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class EmbarcacaoService {
  baseUrl = "http://localhost:8080/embarcacoes";

  constructor(private snackBar: MatSnackBar, private http: HttpClient) {}

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, "X", {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top",
      panelClass: isError ? ["msg-error"] : ["msg-success"],
    });
  }

  create(embarcacao: Embarcacao): Observable<Embarcacao> {
    return this.http.post<Embarcacao>(this.baseUrl, embarcacao).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e)
      )
    );
  }
  
  read(): Observable<Embarcacao[]> {
    return this.http.get<Embarcacao[]>(this.baseUrl).pipe(
      map((obj) => obj),
      catchError((e) => this.errorGet(e))
    );
  }

  readById(id: number): Observable<Embarcacao> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<Embarcacao>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorGet(e))
    );
  }

  update(embarcacao: Embarcacao): Observable<Embarcacao> {
    const url = `${this.baseUrl}/${embarcacao.id}`;
    return this.http.put<Embarcacao>(url, embarcacao).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id: number): Observable<Embarcacao> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<Embarcacao>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandle(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    this.showMessage("Não foi possivel cadastrar embarcação", true);
    return EMPTY;
  }
  errorHandle(e: any): Observable<any> {
    this.showMessage("Não foi possivel excluir embarcação", true);
    return EMPTY;
  }
  errorGet(e: any): Observable<any> {
    this.showMessage("Erro ao carregar dados", true);
    return EMPTY;
  }
}
