import { Embarcacao } from "../embarcacao.model";
import { Router, ActivatedRoute } from "@angular/router";
import { EmbarcacaoService } from "../embarcacao.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-embarcacao-update",
  templateUrl: "./embarcacao-update.component.html",
  styleUrls: ["./embarcacao-update.component.css"],
})
export class EmbarcacaoUpdateComponent implements OnInit {
  embarcacao: Embarcacao;

  constructor(
    private embarcacaoService: EmbarcacaoService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.embarcacaoService.readById(id).subscribe((embarcacao) => {
      this.embarcacao = embarcacao;
    });
  }

  updateEmbarcacao(): void {
    this.embarcacaoService.update(this.embarcacao).subscribe(() => {
      this.embarcacaoService.showMessage("Embarcação atualizada com sucesso!");
      this.router.navigate(["/embarcacao"]);
    });
  }

  cancel(): void {
    this.router.navigate(["/embarcacao"]);
  }
}
