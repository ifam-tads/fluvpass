import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Passagem } from "../passagens.model";
import { PassagemService } from "../passagens.service";

@Component({
  selector: "app-passagens-update",
  templateUrl: "./passagens-update.component.html",
  styleUrls: ["./passagens-update.component.css"],
})
export class PassagemUpdateComponent implements OnInit {
  passagem: Passagem;

  constructor(
    private passagemService: PassagemService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.passagemService.readById(id).subscribe((passagem) => {
      this.passagem = passagem;
    });
  }

  updatePassagem(): void {
    this.passagemService.update(this.passagem).subscribe(() => {
      this.passagemService.showMessage("Passagens atualizada com sucesso!");
      this.router.navigate(["/passagens"]);
    });
  }

  cancel(): void {
    this.router.navigate(["/passagens"]);
  }
}
