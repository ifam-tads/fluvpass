
import { Component, OnInit } from '@angular/core';
import { Passagem } from '../passagens.model';
import { PassagemService } from '../passagens.service';

@Component({
  selector: 'app-passagens-read',
  templateUrl: './passagens-read.component.html',
  styleUrls: ['./passagens-read.component.css']
})
export class PassagemReadComponent implements OnInit {

  passagem: Passagem[]
  displayedColumns = ['id','cpf', 'nomePassageiro','embarcacaoId','inscricao','action']
  
  constructor(private embarcacaoervice: PassagemService) { }

  ngOnInit(): void {
    this.embarcacaoervice.read().subscribe(passagem => {
      this.passagem = passagem
    })
  }

  filtrar(palavraChave: string) {
    if (palavraChave) {
      palavraChave = palavraChave.toUpperCase();

      this.passagem = this.passagem.filter(
        (a) => a.nomePassageiro.toUpperCase().indexOf(palavraChave) >= 0
      );
    }
  }
}
