
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Passagem } from '../passagens.model';
import { PassagemService } from '../passagens.service';

@Component({
  selector: 'app-passagens-create',
  templateUrl: './passagens-create.component.html',
  styleUrls: ['./passagens-create.component.css']
})
export class PassagemCreateComponent implements OnInit {

  passagem: Passagem = {
    idEmbarcacao:null,
    cpfpassageiro: '',
    nomePassageiro:''

  }

  constructor(private passagemService: PassagemService,
      private router: Router) { }

  ngOnInit(): void {    
  }

  createEmbarcacao(): void {
    this.passagemService.create(this.passagem).subscribe(() => {
      this.passagemService.showMessage('Passagem criada com sucesso!')
      this.router.navigate(['/passagens'])
    })
  }

  cancel(): void {
    this.router.navigate(['/passagens'])
  }
}
