import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { HttpClient } from "@angular/common/http";
import { Observable, EMPTY } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Passagem } from "./passagens.model";

@Injectable({
  providedIn: "root",
})
export class PassagemService {
  baseUrl = "http://localhost:8080/passagens";

  constructor(private snackBar: MatSnackBar, private http: HttpClient) {}

  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, "X", {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top",
      panelClass: isError ? ["msg-error"] : ["msg-success"],
    });
  }

  create(product: Passagem): Observable<Passagem> {
    return this.http.post<Passagem>(this.baseUrl, product).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e)
      )
    );
  }

  read(): Observable<Passagem[]> {
    return this.http.get<Passagem[]>(this.baseUrl).pipe(
      map((obj) => obj),
      catchError((e) => this.errorGet(e))
    );
  }

  readById(id: number): Observable<Passagem> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<Passagem>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorGet(e))
    );
  }

  update(product: Passagem): Observable<Passagem> {
    const url = `${this.baseUrl}/${product.id}`;
    return this.http.put<Passagem>(url, product).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  delete(id: number): Observable<Passagem> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<Passagem>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandle(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    this.showMessage("Não foi possivel cadastrar passagem", true);
    return EMPTY;
  }

  errorHandle(e: any): Observable<any> {
    this.showMessage("Não foi possivel Excluir passagem", true);
    return EMPTY;
  }

  errorGet(e: any): Observable<any> {
    this.showMessage("Erro ao carregar dados", true);
    return EMPTY;
  }
}
