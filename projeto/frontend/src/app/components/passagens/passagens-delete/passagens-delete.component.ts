import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Passagem } from "../passagens.model";
import { PassagemService } from "../passagens.service";

@Component({
  selector: "app-passagens-delete",
  templateUrl: "./passagens-delete.component.html",
  styleUrls: ["./passagens-delete.component.css"],
})
export class PassagemDeleteComponent implements OnInit {
  passagem: Passagem;

  constructor(
    private passagemService: PassagemService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.passagemService.readById(id).subscribe((passagem) => {
      this.passagem = passagem;
    });
  }

  deleteEmbarcacao(): void {
    this.passagemService.delete(this.passagem.id).subscribe(() => {
      this.passagemService.showMessage("Passagem excluida com sucesso!");
      this.router.navigate(["/passagens"]);
    });
  }

  cancel(): void {
    this.router.navigate(["/passagens"]);
  }
}
