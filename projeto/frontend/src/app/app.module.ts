import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/template/header/header.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { FooterComponent } from './components/template/footer/footer.component';
import { NavComponent } from './components/template/nav/nav.component';

import { MatSidenavModule } from  '@angular/material/sidenav';
import { MatCardModule } from  '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { HomeComponent } from './views/home/home.component';
import { EmbarcacaoCrudComponent } from './views/embarcacao-crud/embarcacao-crud.component';
import { EmbarcacaoCreateComponent } from './components/embarcacao/embarcacao-create/embarcacao-create.component';
import { MatButtonModule } from  '@angular/material/button';
import { MatSnackBarModule } from  '@angular/material/snack-bar';
import { HttpClientModule } from  '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EmbarcacaoReadComponent } from './components/embarcacao/embarcacao-read/embarcacao-read.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { RedDirective } from './directives/red.directive';

import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from  '@angular/common';
import { ForDirective } from './directives/for.directive';
import { EmbarcacaoUpdateComponent } from './components/embarcacao/embarcacao-update/embarcacao-update.component';
import { EmbarcacaoDeleteComponent } from './components/embarcacao/embarcacao-delete/embarcacao-delete.component';
import { PassagemCreateComponent } from './components/passagens/passagens-create/passagens-create.component';
import { PassagemReadComponent } from './components/passagens/passagens-read/passagens-read.component';
import { PassagemUpdateComponent } from './components/passagens/passagens-update/passagens-update.component';
import { PassagemDeleteComponent } from './components/passagens/passagens-delete/passagens-delete.component';
import { PassagemCrudComponent } from './views/passagens-crud/passagens-crud.component';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    HomeComponent,
    RedDirective,
    ForDirective,
    EmbarcacaoCrudComponent,
    EmbarcacaoCreateComponent,
    EmbarcacaoReadComponent,
    EmbarcacaoUpdateComponent,
    EmbarcacaoDeleteComponent,
    PassagemCrudComponent,
    PassagemCreateComponent,
    PassagemReadComponent,
    PassagemUpdateComponent,
    PassagemDeleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  providers: [{
    provide: LOCALE_ID,
    useValue: 'pt-BR'
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
