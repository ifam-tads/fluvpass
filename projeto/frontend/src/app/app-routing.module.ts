import { EmbarcacaoDeleteComponent } from './components/embarcacao/embarcacao-delete/embarcacao-delete.component';
import { EmbarcacaoUpdateComponent } from './components/embarcacao/embarcacao-update/embarcacao-update.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./views/home/home.component";
import { EmbarcacaoCrudComponent } from "./views/embarcacao-crud/embarcacao-crud.component";
import { EmbarcacaoCreateComponent } from './components/embarcacao/embarcacao-create/embarcacao-create.component';
import { PassagemCrudComponent } from './views/passagens-crud/passagens-crud.component';
import { PassagemCreateComponent } from './components/passagens/passagens-create/passagens-create.component';
import { PassagemUpdateComponent } from './components/passagens/passagens-update/passagens-update.component';
import { PassagemDeleteComponent } from './components/passagens/passagens-delete/passagens-delete.component';


const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "embarcacao",
    component: EmbarcacaoCrudComponent
  },
  {
    path: "embarcacao/create",
    component: EmbarcacaoCreateComponent
  },
  {
    path: "embarcacao/update/:id",
    component: EmbarcacaoUpdateComponent
  },
  {
    path: "embarcacao/delete/:id",
    component: EmbarcacaoDeleteComponent
  },

  {
    path: "passagens",
    component: PassagemCrudComponent
  },
  {
    path: "passagens/create",
    component: PassagemCreateComponent
  },
  {
    path: "passagens/update/:id",
    component: PassagemUpdateComponent
  },
  {
    path: "passagens/delete/:id",
    component: PassagemDeleteComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
