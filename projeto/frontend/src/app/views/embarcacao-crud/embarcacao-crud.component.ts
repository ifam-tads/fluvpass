import { HeaderService } from '../../components/template/header/header.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-embarcacao-crud',
  templateUrl: './embarcacao-crud.component.html',
  styleUrls: ['./embarcacao-crud.component.css']
})
export class EmbarcacaoCrudComponent implements OnInit {

  constructor(private router: Router, private headerService: HeaderService) {
    headerService.headerData = {
      title: 'Cadastro de Passagens',
      icon: 'storefront',
      routeUrl: '/embarcacao'
    }
  }

  ngOnInit(): void {
  }

  navigateToEmbarcacaoCreate(): void {
    this.router.navigate(['/embarcacao/create'])
  }

}
