package com.ifam.fluvpass.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tb_passagem")
public class Passagem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Embarcacao embarcacao;

    private String nomePassgeiro;

    private String cpfPassageiro;

    private String dataViagem;

    public Passagem() {
    }

}
