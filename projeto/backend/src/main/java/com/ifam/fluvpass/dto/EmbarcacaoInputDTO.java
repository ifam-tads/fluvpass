package com.ifam.fluvpass.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class EmbarcacaoInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank
    private String numeroInscricao;

    @NotBlank
    private String nomeEmbarcacao;

    @NotNull
    private Integer qtdPassageiros;

    @NotNull
    private Double valorPassagem;

    @NotBlank
    private String cpfProprietario;

    @NotBlank
    private String nomeProprietario;

}
