package com.ifam.fluvpass.entities;

import com.ifam.fluvpass.dto.EmbarcacaoInputDTO;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tb_embarcacao")
public class Embarcacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String numeroInscricao;

    private String nomeEmbarcacao;

    private Integer qtdPassageiros;

    private Double valorPassagem;

    private String cpfProprietario;

    private String nomeProprietario;

    public Embarcacao() {
    }

    public Embarcacao(EmbarcacaoInputDTO dto) {
        this.numeroInscricao = dto.getNumeroInscricao();
        this.nomeEmbarcacao = dto.getNomeEmbarcacao();
        this.qtdPassageiros = dto.getQtdPassageiros();
        this.valorPassagem = dto.getValorPassagem();
        this.cpfProprietario = dto.getCpfProprietario();
        this.nomeProprietario = dto.getNomeProprietario();
    }

}
