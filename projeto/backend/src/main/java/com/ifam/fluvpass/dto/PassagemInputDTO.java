package com.ifam.fluvpass.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class PassagemInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private Long idEmbarcacao;

    @NotNull
    private String cpfpassageiro;

    @NotNull
    private String nomePassageiro;

    @NotNull
    private String dataViagem;

    public PassagemInputDTO() {
    }
}
