package com.ifam.fluvpass.services;

import com.ifam.fluvpass.dto.PassagemDTO;
import com.ifam.fluvpass.dto.PassagemInputDTO;
import com.ifam.fluvpass.entities.Embarcacao;
import com.ifam.fluvpass.entities.Passagem;
import com.ifam.fluvpass.exceptionhandler.exception.EntidadeNaoEncontradaException;
import com.ifam.fluvpass.repositories.EmbarcacaoRepository;
import com.ifam.fluvpass.repositories.PassagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PassagemService {

    @Autowired
    private PassagemRepository repository;

    @Autowired
    private EmbarcacaoRepository embarcacaoRepository;

    public List<PassagemDTO> findAll() {
        return repository.findAll()
                .stream()
                .map(PassagemDTO::new)
                .collect(Collectors.toList());
    }

    public PassagemDTO cadastro(PassagemInputDTO dto) {

        Embarcacao embarcacao = embarcacaoRepository.findById(dto.getIdEmbarcacao())
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Embarcação não encontrada."));

        Passagem passagem = new Passagem();
        passagem.setEmbarcacao(embarcacao);
        passagem.setCpfPassageiro(dto.getCpfpassageiro());
        passagem.setNomePassgeiro(dto.getNomePassageiro());
        passagem.setDataViagem(dto.getDataViagem());

        return new PassagemDTO(repository.save(passagem));
    }

    public PassagemDTO atualizar(Long id, PassagemInputDTO dto) {
        Embarcacao embarcacao = embarcacaoRepository.findById(dto.getIdEmbarcacao())
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Embarcação não encontrada."));

        Passagem passagem = repository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Passagem não encontrada."));

        passagem.setEmbarcacao(embarcacao);
        passagem.setCpfPassageiro(dto.getCpfpassageiro());
        passagem.setNomePassgeiro(dto.getNomePassageiro());
        passagem.setDataViagem(dto.getDataViagem());

        return new PassagemDTO(repository.save(passagem));
    }

    public void deletar(Long id) {
        Passagem passagem = repository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Passagem não encontrada."));
        repository.delete(passagem);
    }

    public PassagemDTO buscar(Long id) {
        Passagem passagem = repository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Passagem não encontrada."));
        return new PassagemDTO(passagem);
    }
}
