package com.ifam.fluvpass.controllers;

import com.ifam.fluvpass.dto.EmbarcacaoDTO;
import com.ifam.fluvpass.dto.EmbarcacaoInputDTO;
import com.ifam.fluvpass.dto.PassagemDTO;
import com.ifam.fluvpass.dto.PassagemInputDTO;
import com.ifam.fluvpass.services.PassagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/passagens")
public class PassagemController {
    @Autowired
    private PassagemService service;

    @GetMapping
    public ResponseEntity<List<PassagemDTO>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PassagemDTO> buscar(@PathVariable Long id) {
        return ResponseEntity.ok(service.buscar(id));
    }

    @PostMapping
    public ResponseEntity<PassagemDTO> cadastro(@Valid @RequestBody PassagemInputDTO dto) {
        PassagemDTO passagem = service.cadastro(dto);

        URI uri = ServletUriComponentsBuilder.
                fromCurrentRequest().
                path("/{id}").
                buildAndExpand(passagem.getId())
                .toUri();

        return ResponseEntity.created(uri).body(passagem);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PassagemDTO> atualizar(@PathVariable Long id, @Valid @RequestBody PassagemInputDTO dto) {
        return ResponseEntity.ok(service.atualizar(id, dto));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    public void deletar(@PathVariable Long id) {
        service.deletar(id);
    }
}
