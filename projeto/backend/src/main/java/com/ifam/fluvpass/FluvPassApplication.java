package com.ifam.fluvpass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FluvPassApplication {

	public static void main(String[] args) {
		SpringApplication.run(FluvPassApplication.class, args);
	}

}
