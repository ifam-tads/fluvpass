package com.ifam.fluvpass.dto;

import com.ifam.fluvpass.entities.Embarcacao;
import lombok.Data;

import java.io.Serializable;

@Data
public class EmbarcacaoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String numeroInscricao;

    private String nomeEmbarcacao;

    private Integer qtdPassageiros;

    private Double valorPassagem;

    private String cpfProprietario;

    private String nomeProprietario;

    public EmbarcacaoDTO() {
    }

    public EmbarcacaoDTO(Embarcacao embarcacao) {
        this.id = embarcacao.getId();
        this.numeroInscricao = embarcacao.getNumeroInscricao();
        this.nomeEmbarcacao = embarcacao.getNomeEmbarcacao();
        this.qtdPassageiros = embarcacao.getQtdPassageiros();
        this.valorPassagem = embarcacao.getValorPassagem();
        this.cpfProprietario = embarcacao.getCpfProprietario();
        this.nomeProprietario = embarcacao.getNomeProprietario();
    }
}
