package com.ifam.fluvpass.controllers;

import com.ifam.fluvpass.dto.EmbarcacaoDTO;
import com.ifam.fluvpass.dto.EmbarcacaoInputDTO;
import com.ifam.fluvpass.services.EmbarcacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/embarcacoes")
public class EmbarcacaoController {

    @Autowired
    private EmbarcacaoService service;

    @GetMapping
    public ResponseEntity<List<EmbarcacaoDTO>> listar() {
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EmbarcacaoDTO> buscar(@PathVariable Long id) {
        return ResponseEntity.ok(service.buscar(id));
    }

    @PostMapping
    public ResponseEntity<EmbarcacaoDTO> cadastro(@Valid @RequestBody EmbarcacaoInputDTO dto) {
        EmbarcacaoDTO embarcacao = service.cadastro(dto);

        URI uri = ServletUriComponentsBuilder.
                fromCurrentRequest().
                path("/{id}").
                buildAndExpand(embarcacao.getId())
                .toUri();

        return ResponseEntity.created(uri).body(embarcacao);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<EmbarcacaoDTO> atualizar(@PathVariable Long id, @Valid @RequestBody EmbarcacaoInputDTO dto) {
        return ResponseEntity.ok(service.atualizar(id, dto));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    public void deletar(@PathVariable Long id) {
        service.deletar(id);
    }
}
