package com.ifam.fluvpass.services;

import com.ifam.fluvpass.dto.EmbarcacaoDTO;
import com.ifam.fluvpass.dto.EmbarcacaoInputDTO;
import com.ifam.fluvpass.entities.Embarcacao;
import com.ifam.fluvpass.exceptionhandler.exception.EntidadeNaoEncontradaException;
import com.ifam.fluvpass.repositories.EmbarcacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmbarcacaoService {

    @Autowired
    private EmbarcacaoRepository repository;

    public List<EmbarcacaoDTO> findAll() {
        List<EmbarcacaoDTO> embarcacoes = repository.findAll()
                .stream()
                .map(EmbarcacaoDTO::new)
                .collect(Collectors.toList());

        return embarcacoes;
    }

    public EmbarcacaoDTO buscar(Long id) {
        Embarcacao embarcacao = repository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Embarcacão não encontrada."));

        return new EmbarcacaoDTO(embarcacao);
    }

    public EmbarcacaoDTO cadastro(EmbarcacaoInputDTO dto) {
        Embarcacao embarcacao = new Embarcacao(dto);
        return new EmbarcacaoDTO(repository.save(embarcacao));
    }

    public EmbarcacaoDTO atualizar(Long id, EmbarcacaoInputDTO dto) {
        Embarcacao embarcacao = repository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Embarcacão não encontrada."));

        embarcacao.setNomeEmbarcacao(dto.getNomeEmbarcacao());
        embarcacao.setNumeroInscricao(dto.getNumeroInscricao());
        embarcacao.setValorPassagem(dto.getValorPassagem());
        embarcacao.setQtdPassageiros(dto.getQtdPassageiros());
        embarcacao.setCpfProprietario(dto.getCpfProprietario());
        embarcacao.setNomeProprietario(dto.getNomeProprietario());

        return new EmbarcacaoDTO(repository.save(embarcacao));
    }

    public void deletar(Long id) {
        Embarcacao embarcacao = repository.findById(id)
                .orElseThrow(() -> new EntidadeNaoEncontradaException("Embarcacão não encontrada."));
        repository.delete(embarcacao);
    }
}
