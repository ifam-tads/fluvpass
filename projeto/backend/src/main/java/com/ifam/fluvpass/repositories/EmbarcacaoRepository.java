package com.ifam.fluvpass.repositories;

import com.ifam.fluvpass.entities.Embarcacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmbarcacaoRepository extends JpaRepository<Embarcacao, Long> {
}
