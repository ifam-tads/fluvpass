package com.ifam.fluvpass.dto;

import com.ifam.fluvpass.entities.Passagem;
import lombok.Data;

import java.io.Serializable;

@Data
public class PassagemDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String nomeEmbarcacao;

    private String numeroInscricao;

    private String nomePassageiro;

    private String cpfpassageiro;

    private String dataViagem;

    public PassagemDTO() {
    }

    public PassagemDTO(Passagem passagem) {
        this.id = passagem.getId();
        this.nomeEmbarcacao = passagem.getEmbarcacao().getNomeEmbarcacao();
        this.numeroInscricao = passagem.getEmbarcacao().getNumeroInscricao();
        this.nomePassageiro = passagem.getNomePassgeiro();
        this.cpfpassageiro = passagem.getCpfPassageiro();
        this.dataViagem = passagem.getDataViagem();
    }

}
